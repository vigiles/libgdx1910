package com.cuiweiyou.gdx_1_9_10.game;

import com.badlogic.gdx.Game;
import com.cuiweiyou.gdx_1_9_10.screen.DemoScreen1;

public class DemoGame extends Game {

    private DemoScreen1 screen1;

    @Override
    public void create() {
        screen1 = new DemoScreen1();
        setScreen(screen1);
    }

    @Override
    public void dispose() {

        if (null != screen1) {
            screen1.dispose();
            screen1 = null;
        }

        super.dispose(); // 必须的操作
    }
}
