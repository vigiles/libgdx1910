package com.cuiweiyou.gdx_1_9_10.event;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AfterAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.cuiweiyou.gdx_1_9_10.actor.DemoActor1;
import com.cuiweiyou.gdx_1_9_10.tool.PictureTool;

import java.util.Random;

public class Actor1ClickListener extends ClickListener {

    private OnActor1ClickListener onActor1ClickListener;
    private DemoActor1 actor;
    private Music music;

    public Actor1ClickListener(DemoActor1 actor, OnActor1ClickListener listener) {
        this.onActor1ClickListener = listener;
        this.actor = actor;

    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);

        actor.addAction(getRepeatAction()); // 如果这个是forever，那么永远不会执行after
        actor.addAction(getAfterAction());  // 执行3次序列动画后，执行after

        String animal = PictureTool.getInstance().getStaticPicture();
        int i = new Random().nextInt(4) + 1;
        String path = animal + "/" + i + ".png";
        actor.setRegion(path);

        music = Gdx.audio.newMusic(Gdx.files.internal(animal + "/" + animal + ".wav"));
        music.setLooping(false);
        music.play();

        if (null != onActor1ClickListener) {
            onActor1ClickListener.onActor1Click(animal);
        }

        Log.e("ard", "演员" + actor.getName() + "被点击：" + event.toString() + "，x=" + x + "，y=" + y);
    }

    private RepeatAction getRepeatAction() {

        MoveByAction moveActiont = Actions.moveBy( //
                                                   10, //
                                                   10, //
                                                   1f, //
                                                   Interpolation.swingOut);
        ScaleToAction scaleAction = Actions.scaleTo( //
                                                     2.0F,  //
                                                     2.0F,  //
                                                     1f);

        ParallelAction parallelAction = Actions.parallel(moveActiont, scaleAction);

        RunnableAction runnableAction = Actions.run(new Runnable() {
            @Override
            public void run() {
                actor.setPosition(0, 0); // To方法执行后Actor会保持动作结束的状态。还原后才能下次动作效果
                actor.setScale(1);
            }
        });

        SequenceAction sequenceAction = Actions.sequence(parallelAction, runnableAction);  // 放在末尾

        RepeatAction repeatAction = Actions.repeat(2, sequenceAction); // 重复执行
        return repeatAction;
    }

    private AfterAction getAfterAction() {

        return Actions.after(Actions.run(new Runnable() {
            @Override
            public void run() {
                Log.e("ard", "亮瞎眼的Pose");
            }
        }));
    }

    public interface OnActor1ClickListener {
        public void onActor1Click(String animal);
    }
}
