package com.cuiweiyou.gdx_1_9_10.tool;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PictureTool {
    private static PictureTool instance;
    private List<String> animalList;

    private PictureTool() {
        animalList = new ArrayList<>();
        animalList.add("bull");
        animalList.add("cat");
        animalList.add("chicken");
        animalList.add("dog");
        animalList.add("duck");
        animalList.add("goose");
        animalList.add("horse");
        animalList.add("pig");
        animalList.add("sheep");
    }

    public static PictureTool getInstance() {
        if (null == instance) {
            synchronized (PictureTool.class) {
                if (null == instance) {
                    instance = new PictureTool();
                }
            }
        }
        return instance;
    }

    public String getStaticPicture() {
        int index = new Random().nextInt(animalList.size());
        String animal = animalList.get(index);
        return animal;
    }
}
