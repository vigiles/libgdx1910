package com.cuiweiyou.gdx_1_9_10.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.cuiweiyou.gdx_1_9_10.stage.BaseStage;
import com.cuiweiyou.gdx_1_9_10.stage.Stage1;

public class DemoScreen1 extends ScreenAdapter {

    private BaseStage stage1;

    public DemoScreen1() {

        stage1 = new Stage1();

        Gdx.input.setInputProcessor(stage1); // 一次只能有一个stage接收交互事件
    }

    @Override
    public void render(float delta) {
        stage1.render(); // 此时只有一个stage显示。显示哪个stage，不影响哪个stage在发生交互
    }

    @Override
    public void dispose() {
        stage1.dispose();
    }
}
