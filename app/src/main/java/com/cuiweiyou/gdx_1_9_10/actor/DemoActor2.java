package com.cuiweiyou.gdx_1_9_10.actor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class DemoActor2 extends Actor {
    TextureRegion region;

    public DemoActor2(String assets) {
        region = new TextureRegion(new Texture(assets));
        setSize(this.region.getRegionWidth(), this.region.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(region,       //
                   getX(),       //
                   getY(),       //
                   getOriginX(), //
                   getOriginY(), //
                   getWidth(),   //
                   getHeight(),  //
                   getScaleX(),  //
                   getScaleY(),  //
                   getRotation());
    }
}
