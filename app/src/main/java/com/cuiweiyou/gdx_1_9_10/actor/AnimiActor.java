package com.cuiweiyou.gdx_1_9_10.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;

public class AnimiActor extends Actor {
    private Animation<Image> animation;
    private float duration = 0;
    private boolean isAnimitionAble = false;
    private boolean looping;
    private int width = 220;
    private int height = 220;

    public AnimiActor() {

    }

    public void setAnimal(String animal) {
        float frameDuration = 1f;

        TextureRegion textureRegion1 = new TextureRegion(new Texture(animal + "/1.png"));
        TextureRegion textureRegion2 = new TextureRegion(new Texture(animal + "/2.png"));
        TextureRegion textureRegion3 = new TextureRegion(new Texture(animal + "/3.png"));
        TextureRegion textureRegion4 = new TextureRegion(new Texture(animal + "/4.png"));

        int maxW = 0;
        int maxH = 0;
        int w1 = textureRegion1.getRegionWidth();
        int h1 = textureRegion1.getRegionHeight();
        maxW = Math.max(maxW, w1);
        maxH = Math.max(maxH, h1);
        int w2 = textureRegion2.getRegionWidth();
        int h2 = textureRegion2.getRegionHeight();
        maxW = Math.max(maxW, w2);
        maxH = Math.max(maxH, h2);
        int w3 = textureRegion3.getRegionWidth();
        int h3 = textureRegion3.getRegionHeight();
        maxW = Math.max(maxW, w3);
        maxH = Math.max(maxH, h3);
        int w4 = textureRegion4.getRegionWidth();
        int h4 = textureRegion4.getRegionHeight();
        maxW = Math.max(maxW, w4);
        maxH = Math.max(maxH, h4);

        Image image1 = new Image(textureRegion1);
        image1.setScaling(Scaling.fit);
        Image image2 = new Image(textureRegion2);
        image2.setScaling(Scaling.fit);
        Image image3 = new Image(textureRegion3);
        image3.setScaling(Scaling.fit);
        Image image4 = new Image(textureRegion4);
        image4.setScaling(Scaling.fit);
        Array<Image> keyFrames = new Array<>();
        keyFrames.add(image1);
        keyFrames.add(image2);
        keyFrames.add(image3);
        keyFrames.add(image4);
        Animation.PlayMode playMode = Animation.PlayMode.LOOP;
        looping = true; // 和PlayMode.LOOP一致
        animation = new Animation(frameDuration, keyFrames, playMode);

        setSize(maxW, maxH);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        Image currentFrame = animation.getKeyFrame(this.duration, looping); // 根据播放模式和播放时长获取当前关键帧。即此刻应该播放哪一帧
        TextureRegionDrawable drawable = (TextureRegionDrawable) currentFrame.getDrawable();
        TextureRegion textureRegion = drawable.getRegion();
        batch.draw(textureRegion,       //
                   getX(),       //
                   getY(),       //
                   getOriginX(), //
                   getOriginY(), //
                   getWidth(),   //
                   getHeight(),  //
                   getScaleX(),  //
                   getScaleY(),  //
                   getRotation());

        if (!isAnimitionAble) { // 动画被暂停了
            // duration = 0; // 动画被停止了
            return;
        }

        float graphicsDeltaTime = Gdx.graphics.getDeltaTime(); // 相对上一帧的耗时，秒
        this.duration += graphicsDeltaTime;  // 动画已经播放时长
        if (this.duration >= Float.MAX_VALUE) {
            this.duration = 0;
        }

    }

    public void setAnimationAble(boolean isAnimi) {
        this.isAnimitionAble = isAnimi;
    }

    public boolean isAnimiting() {
        if (looping) {
            return isAnimitionAble;
        }

        float duration22 = animation.getFrameDuration();     // 每帧动画的长度秒。即初始设置的frameDuration
        float duration33 = animation.getAnimationDuration(); // 播放一次完整动画的长度秒。即frameDuration*帧数

        return duration >= animation.getAnimationDuration();
    }
}
