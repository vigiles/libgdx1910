package com.cuiweiyou.gdx_1_9_10.stage;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class BaseStage extends Stage {

    public void render() {
        act();
        draw();
    }
}
