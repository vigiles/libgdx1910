package com.cuiweiyou.gdx_1_9_10.stage;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.cuiweiyou.gdx_1_9_10.actor.DemoActor2;

public class Stage2 extends BaseStage {

    private Actor actor;

    public Stage2() {
        actor = new DemoActor2("badlogic.jpg");
        actor.setName("2号");
        actor.setPosition(100, 100);
        addActor(actor);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        Log.e("ard", "舞台2被点击：screenX=" + screenX + "，screenY=" + screenY + "，pointer=" + pointer + "，button=" + button);


        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public void render() {

        Gdx.gl.glClearColor(0, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
    }

    @Override
    public void dispose() {
        actor.clear();

        super.dispose();
    }
}
