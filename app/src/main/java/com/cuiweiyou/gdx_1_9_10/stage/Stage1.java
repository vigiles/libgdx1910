package com.cuiweiyou.gdx_1_9_10.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.cuiweiyou.gdx_1_9_10.actor.AnimiActor;
import com.cuiweiyou.gdx_1_9_10.actor.DemoActor1;
import com.cuiweiyou.gdx_1_9_10.event.Actor1ClickListener;
import com.cuiweiyou.gdx_1_9_10.particle.FireParticle;
import com.cuiweiyou.gdx_1_9_10.tool.PictureTool;

import java.util.Random;

public class Stage1 extends BaseStage {

    private DemoActor1 staticActor;
    private AnimiActor animiActor;
    private float red = 1;
    private float green = 1;
    private float blue = 1;
    private float alpha = 1;
    private float stageWidth;
    private float stageHeight;
    private boolean directionWidthForward = true;
    private boolean directionHeightForward = true;
    private int moveWidthStep = 4;
    private int moveHeightStep = 6;
    private boolean isNotJustOnlyStageClicked;

    private Music backgroundMusic;

    public Stage1() {
        String animal = PictureTool.getInstance().getStaticPicture();
        int i = new Random().nextInt(4) + 1;
        String path = animal + "/" + i + ".png";
        staticActor = new DemoActor1(path);
        staticActor.setPosition(0, 0);
        staticActor.setName("一号");

        animiActor = new AnimiActor();
        animiActor.setPosition(200, 200);
        animiActor.setAnimal(animal);
        animiActor.setAnimationAble(true);

        addActor(staticActor);
        addActor(animiActor);

        int bgmIndex = new Random().nextInt(3) + 1;
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("bgm/" + bgmIndex + ".wav")); // 底层为安卓的SoundPool
        //backgroundMusic.setPan(soundId, -1, 1);  // pan-声道【-1全左 - 1全右】，volume音量【0 - 1】根据id设置声道音量
        //backgroundMusic.setPitch(soundId, 1.2f); // 播放速度。默认1, 大于1会变快-尖锐，反之慢-低沉。取值0.5-2
        //backgroundMusic.setVolume(soundId, 1f);  // 音量。
        backgroundMusic.setLooping(true); // soud.play后返回播放线程的id，可以据此控制其是否循环播放
        backgroundMusic.play();

        stageWidth = getWidth();
        stageHeight = getHeight();

        staticActor.addListener(new Actor1ClickListener(staticActor, new Actor1ClickListener.OnActor1ClickListener() {
            @Override
            public void onActor1Click(String animal) {
                animiActor.setAnimal(animal);
            }
        }));
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        isNotJustOnlyStageClicked = super.touchDown(screenX, screenY, pointer, button);

        if (isNotJustOnlyStageClicked) { // 某个添加了事件监听的actor被点击到了
            animiActor.setAnimationAble(false);

            if (backgroundMusic.isPlaying()) {
                backgroundMusic.pause();
            }
        } else {
            float ratioR = new Random().nextInt(10) * 0.1f;
            float ratioG = new Random().nextInt(10) * 0.1f;
            float ratioB = new Random().nextInt(10) * 0.1f;
            red = ratioR;
            green = ratioG;
            blue = ratioB;

            animiActor.setAnimationAble(true);

            addActor(new FireParticle(screenX, stageHeight - screenY));

            if (!backgroundMusic.isPlaying()) {
                backgroundMusic.play();
            }
        }

        return isNotJustOnlyStageClicked;
    }

    @Override
    public void dispose() {
        staticActor.clear();
        animiActor.clear();
        backgroundMusic.stop();
        backgroundMusic.dispose();
        super.dispose();
    }

    @Override
    public void render() {

        Gdx.gl.glClearColor(red, green, blue, alpha);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!isNotJustOnlyStageClicked) {
            flushPosition();
        }

        super.render();
    }

    private void flushPosition() {
        float x = animiActor.getX();
        float y = animiActor.getY();

        if (directionWidthForward) {
            x += moveWidthStep;
            if (x >= stageWidth - animiActor.getWidth()) {
                directionWidthForward = false;
            }
        } else {
            x -= moveWidthStep;
            if (x <= 0) {
                directionWidthForward = true;
            }
        }

        if (directionHeightForward) {
            y += moveHeightStep;
            if (y >= stageHeight - animiActor.getHeight()) {
                directionHeightForward = false;
            }
        } else {
            y -= moveHeightStep;
            if (y <= 0) {
                directionHeightForward = true;
            }
        }

        animiActor.setPosition(x, y);
    }
}
